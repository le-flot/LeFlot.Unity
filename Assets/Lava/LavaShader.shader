﻿Shader "Custom/LavaShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _NormalMap("NormalMap", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _GlobalTime("GlobalTime", Float) = 0.0
        _EmitColor("EmissiveColor", Color) = (.2, 0.07, 0.01, 1)
        _HotLavaScale("HotLavaScale", Float) = 1.0
        _HotLavaStrenght("HotLavaStrenght", Float) = 1.0
        _HotLavaPower("HotLavaPower", Float) = 1.4
        _GlobalSpeed("GlobalSpeed", Vector) = (0.0, 0.10, 0, 0)
        _NoiseScale("NoiseScale", Float) = (4.0 , 3.0, 0 , 0)
        _NoiseDir("NoiseDir", Vector) = (0.2 , 0.1, 0 , 0)
        _NoiseStrength("NoiseStrength", Float) = 0.15
        _EmissiveRatio("EmissiveRatio", Range(0,1)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM

        float _GlobalTime;
        float4 _EmitColor;
        float _HotLavaScale;
        float _HotLavaStrenght;
        float _HotLavaPower;
        float4 _GlobalSpeed;
        float4 _NoiseScale;
        float4 _NoiseDir;
        float _NoiseStrength;
        float _EmissiveRatio;

        #define time _GlobalTime * 0.1
        float hash21(in float2 n) { return frac(sin(dot(n, float2(12.9898, 4.1414))) * 43758.5453); }
        float2x2 makem2(in float theta) { float c = cos(theta); float s = sin(theta); return float2x2(c,-s,s,c); }
        float hash12(float2 p)
        {
            float3 p3 = frac(float3(p.xyx) * .1031);
            p3 += dot(p3, p3.yzx + 33.33);
            return frac((p3.x + p3.y) * p3.z);
        }

        float noise(float2 n)
        {
            const float2 d = float2(0.0, 1.0);
            float2 b = floor(n), f = smoothstep(float2(0, 0), float2(1, 1), frac(n));
            return lerp(lerp(hash12(b), hash12(b + d.yx), f.x), lerp(hash12(b + d.xy), hash12(b + d.yy), f.x), f.y);
        }

        float2 gradn(float2 p)
        {
            float ep = .09;
            float gradx = noise(float2(p.x + ep,p.y)) - noise(float2(p.x - ep,p.y));
            float grady = noise(float2(p.x,p.y + ep)) - noise(float2(p.x,p.y - ep));
            return float2(gradx,grady);
        }

        float flow(in float2 p)
        {
            float z = 2.;
            float rz = 0.;
            float2 bp = p;
            for (float i = 1.; i < 6.; i++)
            {
                //primary flow speed
                //p += time*.6 * float2(0,1);

                //secondary flow speed (speed of the perceived flow)
                bp += time * 1.9 * float2(0.0, 0.25);

                //displacement field (try changing time multiplier)
                float2 gr = gradn(i * p * .34 + time * 1.);

                //rotation of the displacement field

                float2x2 m = makem2(time * 6. - (0.05 * p.x + 0.03 * p.y) * 40.);
                gr = mul(gr, m);
                //gr *= makem2(time * 6. - (0.05 * p.x + 0.03 * p.y) * 40.);

                //displace the system
                p += gr * .5;

                //add noise octave
                rz += (sin(noise(p) * 7.) * 0.5 + 0.5) / z;

                //blend factor (blending displaced system with base system)
                //you could call this advection factor (.5 being low, .95 being high)
                p = lerp(bp,p,.77);

                //intensity scaling
                z *= 1.4;
                //octave scaling
                p *= 2.;
                bp *= 1.9;
            }
            return rz;
        }


        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        sampler2D _NormalMap;

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            float2 uv = IN.uv_MainTex;
            uv = uv + _GlobalSpeed.xy * _GlobalTime;
            uv += noise(IN.uv_MainTex * _NoiseScale.xy + _GlobalTime * _NoiseDir.xy) * _NoiseStrength;
            fixed4 n = tex2D(_NormalMap, uv);
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            float rz = flow(uv  * _HotLavaScale);
            float3 col = _EmitColor / rz;
            col = pow(col * _HotLavaStrenght, float3(_HotLavaPower, _HotLavaPower, _HotLavaPower));
            o.Emission = col * _EmissiveRatio;
            o.Albedo += col * (1.0 - _EmissiveRatio);
            o.Normal = n;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
