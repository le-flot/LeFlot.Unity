/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CLOCHE0 = 3396827937U;
        static const AkUniqueID CLOCHE1 = 3396827936U;
        static const AkUniqueID CLOCHE2 = 3396827939U;
        static const AkUniqueID CLOCHE3 = 3396827938U;
        static const AkUniqueID CLOCHE4 = 3396827941U;
        static const AkUniqueID CLOCHE5 = 3396827940U;
        static const AkUniqueID CLOCHE6 = 3396827943U;
        static const AkUniqueID CLOCHE7 = 3396827942U;
        static const AkUniqueID CLOCHE8 = 3396827945U;
        static const AkUniqueID CLOCHE9 = 3396827944U;
        static const AkUniqueID CLOCHE10 = 3658928976U;
        static const AkUniqueID CLOCHE11 = 3658928977U;
        static const AkUniqueID CLOCHE12 = 3658928978U;
        static const AkUniqueID SLIDE = 3686556480U;
        static const AkUniqueID SPECIAL0 = 1354610174U;
        static const AkUniqueID SPECIAL1 = 1354610175U;
        static const AkUniqueID SPECIAL2 = 1354610172U;
        static const AkUniqueID SPECIAL3 = 1354610173U;
        static const AkUniqueID SPECIAL4 = 1354610170U;
        static const AkUniqueID SPECIAL5 = 1354610171U;
        static const AkUniqueID SPECIAL6 = 1354610168U;
        static const AkUniqueID START_FLOW = 3548755022U;
        static const AkUniqueID START_LAVA = 1328167800U;
        static const AkUniqueID START_MUSIC = 540993415U;
        static const AkUniqueID STOP_FLOW = 2711619440U;
        static const AkUniqueID STOP_LAVA = 1256666194U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace NBRPERSONS
        {
            static const AkUniqueID GROUP = 1253229373U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PERSON0 = 2451239314U;
                static const AkUniqueID PERSON1 = 2451239315U;
                static const AkUniqueID PERSON2 = 2451239312U;
                static const AkUniqueID PERSON3 = 2451239313U;
                static const AkUniqueID PERSON4 = 2451239318U;
            } // namespace STATE
        } // namespace NBRPERSONS

        namespace THEME
        {
            static const AkUniqueID GROUP = 1319017392U;

            namespace STATE
            {
                static const AkUniqueID CHRISTMAS = 1268213827U;
                static const AkUniqueID HALLOWEEN = 804732334U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID WATER = 2654748154U;
            } // namespace STATE
        } // namespace THEME

    } // namespace STATES

    namespace SWITCHES
    {
        namespace RE_LAB
        {
            static const AkUniqueID GROUP = 1655312750U;

            namespace SWITCH
            {
                static const AkUniqueID LAB = 578926554U;
                static const AkUniqueID RE = 1601639154U;
            } // namespace SWITCH
        } // namespace RE_LAB

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_OBJECTSPEED = 936725103U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID LEFLOT_SOUNDBANK = 3301967637U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID BELLS = 401578565U;
        static const AkUniqueID FREAK = 1379512410U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SPECIAL = 3064974266U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID DELAY = 357718954U;
        static const AkUniqueID DELAY_PULSE = 3965008756U;
        static const AkUniqueID REVERB = 348963605U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID AUXILIARY = 2396789687U;
        static const AkUniqueID CONTROLLER_SPEAKER = 1334442663U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
