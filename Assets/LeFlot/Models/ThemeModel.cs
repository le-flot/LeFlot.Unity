using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace LeFlot.Models
{
    [Serializable]
    public class ThemeModel
    {
        public int id;

        public string name;

        public List<string> objectNames;

        public override string ToString()
        {
            return JsonUtility.ToJson(this, true);
        }
    }
}
