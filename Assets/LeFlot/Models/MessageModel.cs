using System;
using UnityEngine;

namespace LeFlot.Models
{
    [Serializable]
    public class MessageModel : IEquatable<MessageModel>
    {
        public int id;

        public string text;

        public int objectId;

        public string createdAt;

        bool IEquatable<MessageModel>.Equals(MessageModel other)
        {
            if (other == null)
            {
                return false;
            }

            return this.id == other.id;
        }

        public override bool Equals(object obj) => Equals(obj as MessageModel);
        public override int GetHashCode() => id.GetHashCode();

        public override string ToString()
        {
            return JsonUtility.ToJson(this, true);
        }
    }
}
