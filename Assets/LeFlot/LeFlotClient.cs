using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using LeFlot.Models;

namespace LeFlot
{
    public class LeFlotClient : MonoBehaviour
    {
        public string baseUrl = "https://leflot.art/api/";
        public float currentThemeCheckInterval = 5.0f; // in seconds.
        public float messageCheckInterval = 5.0f; // in seconds.

        public ThemeModel currentTheme;
        public List<ThemeModel> themes = new List<ThemeModel>();
        public List<MessageModel> messages = new List<MessageModel>();

        public bool IsReady { get; private set; }

        private void Start()
        {
            StartCoroutine(StartCoroutines());
        }

        private Uri GetApiUrl(string relativeUrl)
        {
            return new Uri(baseUrl + relativeUrl.TrimStart('/'));
        }

        private IEnumerator StartCoroutines()
        {
            yield return GetCurrentTheme((outCurrentTheme) =>
            {
                if (outCurrentTheme != null)
                {
                    currentTheme = outCurrentTheme;
                    IsReady = true;

                    StartCoroutine(GetCurrentThemeLoop(messageCheckInterval));
                    StartCoroutine(GetMessagesLoop(messageCheckInterval));
                }
            });
        }

        private IEnumerator GetCurrentTheme(Action<ThemeModel> callback)
        {
            var uri = GetApiUrl("themes/current");
            using (var request = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                yield return request.SendWebRequest();
                if (request.isHttpError || request.isNetworkError)
                {
                    Debug.LogWarning($"Could not get current theme: {(request.isHttpError ? $"status code {request.responseCode}" : "network error")}");
                    callback(null);
                    yield break;
                }

                // Get theme.
                var result = request.downloadHandler.text;
                var currentTheme = JsonUtility.FromJson<ThemeModel>(result);

                callback(currentTheme);
            }
        }

        private IEnumerator GetCurrentThemeLoop(float waitTime)
        {
            while (true)
            {
                yield return GetCurrentTheme((outCurrentTheme) =>
                {
                    currentTheme = outCurrentTheme;
                });

                // Wait until next check.
                yield return new WaitForSeconds(waitTime);
            }
        }

        private IEnumerator GetThemes()
        {
            var uri = GetApiUrl("themes");
            using (var request = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                yield return request.SendWebRequest();
                if (request.isHttpError || request.isNetworkError)
                {
                    Debug.LogWarning($"Could not get themes: {(request.isHttpError ? $"status code {request.responseCode}" : "network error")}");
                    yield break;
                }

                // Get themes DTO.
                var result = request.downloadHandler.text;
                var dto = JsonUtility.FromJson<ThemesModel>(result);

                // Get themes.
                themes = dto.data;
            }
        }

        private IEnumerator GetMessagesLoop(float waitTime)
        {
            while (true)
            {
                var uri = GetApiUrl("messages");
                using (var request = UnityWebRequest.Get(uri))
                {
                    // Request and wait for the desired page.
                    yield return request.SendWebRequest();
                    if (request.isHttpError || request.isNetworkError)
                    {
                        Debug.LogWarning($"Could not get messages: {(request.isHttpError ? $"status code {request.responseCode}" : "network error")}");
                        yield break;
                    }

                    // Get objects DTO.
                    var result = request.downloadHandler.text;
                    var dto = JsonUtility.FromJson<MessagesModel>(result);

                    // Get objects.
                    messages = dto.data;
                }

                // Wait until next check.
                yield return new WaitForSeconds(waitTime);
            }
        }
    }
}
