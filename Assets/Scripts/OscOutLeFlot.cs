﻿using UnityEngine;

public class OscOutLeFlot : MonoBehaviour
{
    [SerializeField] OscOut _oscOut;
    [SerializeField] OscIn _oscIn;

    void Start()
    {
        if (!_oscOut)
        {
            _oscOut = gameObject.AddComponent<OscOut>();
        }

        if (!_oscIn)
        {
            _oscIn = gameObject.AddComponent<OscIn>();
        }

        _oscOut.Open(_oscOut.port);
    }
}
