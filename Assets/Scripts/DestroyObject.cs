﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour
{
    public LeFlotObjectSpawner spawner;

    private Light _light;
    private bool _destroyFlag;

    private void Start()
    {
        StartCoroutine(TestImmobile());
    }

    private void Update()
    {
        if (_destroyFlag)
        {
            return;
        }

        if ((transform.position.x > spawner.maxX || transform.position.y < -2))
        {
            _destroyFlag = true;

            //Debug.Log("before fade");
            iTween.Stop(GetComponentInChildren<SendCollisions>().gameObject);

            _light = GetComponentInChildren<Light>();
            if (_light != null)
            {
                // Debug.Log("Must be destroyed fadingly");

                // Fade-out the light before destroying the object
                iTween.ValueTo(gameObject, iTween.Hash(
                    "from", _light.intensity,
                    "to", 0,
                    "onupdate", nameof(CallbackFadeLight),
                    "time", 1,
                    "oncomplete", nameof(CallbackDestroyObject)));
            }
            else
            {
                StartCoroutine(CallbackDestroyObject());
            }
        }
    }

    private IEnumerator CallbackDestroyObject()
    {
        yield return new WaitForSeconds(.2f);
        //Debug.Log("destroy");
        if (TryGetComponent<SlideSound>(out SlideSound slideSound))
        {
            slideSound.stopSound();
        }
        if (spawner != null)
        {
            var component = gameObject.GetComponent<SpawnedModel>();
            spawner.DestroySpawnedModel(component);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void CallbackFadeLight(float value)
    {
        _light.intensity = value;
    }

    private IEnumerator TestImmobile()
    {
        while (!_destroyFlag)
        {
            var lastPosition = transform.position;

            const float waitTime = 10.0f;
            yield return new WaitForSeconds(waitTime);

            // Test if the object has moved in the last 10 seconds.
            if (Vector3.Distance(transform.position, lastPosition) < spawner.immobileThreshold)
            {
                //Debug.Log($"Destroyed spawned object \"{name}\" because it was immobile for {waitTime} seconds");
                StartCoroutine(CallbackDestroyObject());
            }
        }
    }
}
