﻿using UnityEngine;

public class PersonMovement : MonoBehaviour
{
    public float movementSpeed = 3.0f;

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        transform.position = transform.position + move * movementSpeed * Time.deltaTime;
    }
}
