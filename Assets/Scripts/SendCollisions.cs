﻿using UnityEngine;
using System.Collections;

public class SendCollisions : MonoBehaviour
{
    private MainScript _mainScript;
    public int soundPitch;
    private Light _light;
    private bool _limit = true;
    public float baseLightIntensity;
    public float flashIntensity;
    public bool customTag;
    private Vector3 _previousPosition;

    void Start()
    {
        _mainScript = GameObject.Find("MainScript").GetComponent<MainScript>();
        _light = GetComponentInChildren<Light>();
    }

    public void FadeIn()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", 0,
            "to", baseLightIntensity,
            "onupdate", nameof(CallbackFade),
            "time", 1));
    }

    private void CallbackFade(float value)
    {
        if (_light)
        {
            _light.intensity = value;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "person")
        {
            var distance = Vector3.Distance(transform.position, _previousPosition);
            if (distance > _mainScript.distanceLimitCollision)
            {
                //allowing collisions if enough displacement since last collision
                _previousPosition = transform.position;
                
                CollisionFunc();
            }
        }
        else
        {
            //Debug.Log("collision not with a person");
        }
    }

    void CollisionFunc()
    {
        if (!customTag)
        {
            _light.enabled = true;
        }
        //make a flash
        iTween.Stop(gameObject);
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", flashIntensity,
            "to", baseLightIntensity,
            "onupdate", nameof(ChangeIntensity),
            "time", _mainScript.flashLength,
            "oncomplete", nameof(disableLight)));

        _mainScript.SendSound(soundPitch, gameObject, customTag);
    }

    private void ChangeIntensity(float value)
    {
        if (_light)
        {
            _light.intensity = value;
        }
    }

    private void disableLight()
    {
        
        if (!customTag)
        {
            _light.enabled = false;
        }
    }
}
