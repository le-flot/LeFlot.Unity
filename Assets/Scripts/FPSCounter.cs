﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    public Text fpsDisplay;
    public Text averageFPSDisplay;
    int framesPassed = 0;
    float fpsTotal = 0f;
    float meanfps = 0f;
    public Text minFPSDisplay, maxFPSDisplay;
    float minFPS = Mathf.Infinity;
    float maxFPS = 0f;
    int counter = 0;
    int average = 30;

    void Start() {
        //Application.targetFrameRate = 60;
    }

    void Update() {
        float fps = 1 / Time.unscaledDeltaTime;
        counter += 1;
        if (counter == average)
        {
            meanfps = fpsTotal / average;
            fpsDisplay.text = "aver: " + meanfps + "\n min: " + minFPS;
            fpsTotal = 0;
            counter = 0;
            minFPS = 100;
        }
        fpsTotal += fps;
        framesPassed++;
        //averageFPSDisplay.text = "" + (fpsTotal / framesPassed);
        /*
        if (fps > maxFPS && framesPassed > 10) {
            maxFPS = fps;
            maxFPSDisplay.text = "Max: " + maxFPS;
        }
        */
        if (fps < minFPS && framesPassed > 10) {
            minFPS = fps;
            //minFPSDisplay.text = "Min: " + minFPS;
        }
        
    }

}
