﻿using UnityEngine;
using System.Linq;
using LeFlot;

public class MainScript : MonoBehaviour
{
    public ThemeManager themeManager;

    [Header("Control of the camera")]
    public int NbrActivePerson = 0;
    private int _nbrActivePersonPrev = -1;

    [Header("Wwise links")]
    public AK.Wwise.Event[] bellEvent;
    public AK.Wwise.Event[] specialEvent;
    public AK.Wwise.State[] nbrPersonsState;
    public bool riverSound = true;
    public bool music = true;
    public bool bells = true;

    [Space(20)]

    public Camera captureCamera;

    [Space(20)]

    public float flashLength;
    public float interpolationTime;
    public float distanceLimitCollision;
    public LeFlotObjectSpawner spawner;

    void Start()
    {
        Application.targetFrameRate = 40;

        themeManager.OnThemeChanged += OnThemeChanged;

        nbrPersonsState[0].SetValue();
        if (music)
        {
            AkSoundEngine.PostEvent("start_music", gameObject);
        }
    }

    void Update()
    {
#if !UNITY_EDITOR_LINUX
        // to keep sound sounding while the runtime in background or minimized
        AkSoundEngine.WakeupFromSuspend();
        AkSoundEngine.RenderAudio();
#endif // !UNITY_EDITOR_LINUX

        if (Input.GetKeyDown(KeyCode.V)) // quit
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        if (Input.GetKeyDown(KeyCode.T)) // activate/desactivate references for video mapping
        {
            captureCamera.LayerCullingToggle("guides");
            captureCamera.LayerCullingToggle("capturedScenery");
        }

        // override the nbrActivePerson for testing
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            NbrActivePerson = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            NbrActivePerson = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            NbrActivePerson = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            NbrActivePerson = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            NbrActivePerson = 4;
        }

        if (NbrActivePerson != _nbrActivePersonPrev)
        {
            _nbrActivePersonPrev = NbrActivePerson;
            //Debug.Log($"Number of people present: {NbrActivePerson}");

#if !UNITY_EDITOR_LINUX
            var clampedNbrActivePerson = Mathf.Clamp(NbrActivePerson, 0, 4);
            nbrPersonsState[clampedNbrActivePerson].SetValue();
#endif // !UNITY_EDITOR_LINUX

            OnNbrActivePersonChanged();
        }
    }

    public void SendSound(int pitch, GameObject collider, bool special = false)
    {
        if (bells)
        {
            var wwiseEvent = special ? specialEvent : bellEvent;
            wwiseEvent[pitch].Post(collider);
        }
    }

    private void OnThemeChanged()
    {
        // Notice the theme.
        themeManager.CurrentTheme?.SetNbrActivePerson(NbrActivePerson);

        themeManager.CurrentTheme?.SetThemeMusic();

        if (riverSound)
        {
            themeManager.CurrentTheme?.SetFlow();
        }

        //change Madmapper scene
        var sceneNbr = (int)themeManager.CurrentTheme?.madmapperScene;
        themeManager.setMadmapperScene(sceneNbr);
    }

    private void OnNbrActivePersonChanged()
    {
        // Notice the theme.
        themeManager.CurrentTheme?.SetNbrActivePerson(NbrActivePerson);

        // Notice the spawner.
        spawner.SetNbrActivePerson(NbrActivePerson);
    }
}

