using UnityEngine;
using TMPro;

public class SpawnedModel : MonoBehaviour
{
    public GameObject TextObject;

    private TextMeshPro _tmpComponent;

    private void Start()
    {
        _tmpComponent = TextObject?.GetComponent<TextMeshPro>();
    }

    public void ChangeTextColor(Color color, float time)
    {
        if (_tmpComponent == null)
        {
            return;
        }

        iTween.ValueTo(gameObject, iTween.Hash(
            "from", _tmpComponent.color,
            "to", color,
            "onupdate", nameof(CallBackChangeTextColor),
            "time", time));
    }

    private void CallBackChangeTextColor(Color color)
    {
        _tmpComponent.color = color;
    }
}
