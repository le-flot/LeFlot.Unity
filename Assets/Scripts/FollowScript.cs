﻿using UnityEngine;
using System.Collections;

public class FollowScript : MonoBehaviour
{
    public GameObject toFollow;
    public Vector3 offset;

    void Update()
    {
        if (toFollow)
        {
            transform.position = toFollow.transform.position + offset;
        }
        else
        {
            StartCoroutine(DestroyItself());
        }
    }

    private IEnumerator DestroyItself()
    {
        iTween.Stop(gameObject);
        yield return new WaitForSeconds(.2f);
        Destroy(gameObject);
    }
}
