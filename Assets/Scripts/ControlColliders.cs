﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlColliders : MonoBehaviour
{
    public RenderTexture liveFeed;
    public GameObject spawnObject;
    private Texture2D _convertedTex2D;
    public float threshColor;
    public GameObject feedPlane;
    private GameObject[,] _collidersArray;
    public GameObject grid;
    public float maxElevation;

    void Start()
    {
        CreateLiveFeedTexture();
        CreateColliderObjects();
    }

    private void CreateLiveFeedTexture()
    {
        _convertedTex2D = new Texture2D(liveFeed.width, liveFeed.height, TextureFormat.R8, false, true);
        var colors = new Color32[liveFeed.width * liveFeed.height];
        var initColor = new Color32(0, 0, 0, 255);
        for (var i = 0; i < colors.Length; ++i)
        {
            colors[i] = initColor;
        }

        _convertedTex2D.SetPixels32(colors, 0);
        _convertedTex2D.Apply();
    }

    private void CreateColliderObjects()
    {
        var planeWidth = feedPlane.transform.localScale.x;
        var planeHeight = feedPlane.transform.localScale.z;

        var liveFeedToPlaneWidthRatio = planeWidth / (float)liveFeed.width;
        var liveFeedToPlaneHeightRatio = planeHeight / (float)liveFeed.height;

        var basePos = new Vector3(
            -planeWidth / 2.0f,
            feedPlane.transform.position.y,
            -planeHeight / 2.0f);

        _collidersArray = new GameObject[liveFeed.width, liveFeed.height];
        for (var x = 0; x < liveFeed.width; ++x)
        {
            for (var z = 0; z < liveFeed.height; ++z)
            {
                var gameObj = Instantiate(spawnObject, grid.transform).gameObject;
                gameObj.SetActive(false);
                gameObj.transform.localPosition = new Vector3(
                    -planeWidth / 2.0f + x * liveFeedToPlaneWidthRatio,
                    feedPlane.transform.position.y,
                    -planeHeight / 2.0f + z * liveFeedToPlaneHeightRatio);
                gameObj.transform.localScale = new Vector3(
                    liveFeedToPlaneWidthRatio,
                    maxElevation,
                    liveFeedToPlaneHeightRatio);
                gameObj.transform.parent = grid.transform;

                _collidersArray[x, z] = gameObj;
            }
        }
    }

    void Update()
    {
        GenerateShape();
    }

    private void OnPostRender()
    {
        RenderTexture.active = liveFeed;
        _convertedTex2D.ReadPixels(new Rect(0, 0, liveFeed.width, liveFeed.height), 0, 0, false);
        _convertedTex2D.Apply(false);
    }

    void GenerateShape()
    {
        var pixels = _convertedTex2D.GetPixels32(0);
        for (var y = 0; y < _convertedTex2D.height; ++y)
        {
            for (var x = 0; x < _convertedTex2D.width; ++x)
            {
                var gameObj = _collidersArray[x, y];
                var colorIntensity = (float)pixels[x + y * _convertedTex2D.width].r / 255.0f;
                if (colorIntensity < threshColor)
                {
                    gameObj.SetActive(false);
                }
                else
                {
                    var yScale = Mathf.Lerp(threshColor, 1.0f, colorIntensity) * maxElevation;
                    gameObj.transform.localScale = new Vector3(
                        gameObj.transform.localScale.x,
                        yScale,
                        gameObj.transform.localScale.z);
                    gameObj.SetActive(true);
                }
            }
        }
    }
}
