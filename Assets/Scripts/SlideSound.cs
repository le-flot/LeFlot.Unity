﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideSound : MonoBehaviour
{
    private Rigidbody _rb;
    private float _speed;
    private uint _playingID;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _playingID = AkSoundEngine.PostEvent("slide", gameObject);
    }


    void Update()
    {
        _speed = _rb.velocity.magnitude;
        AkSoundEngine.SetRTPCValue("RTPC_objectSpeed", _speed, gameObject);
    }

    public void stopSound()
    {
        AkSoundEngine.StopPlayingID(_playingID, 2500, AkCurveInterpolation.AkCurveInterpolation_Linear);
    }
}
