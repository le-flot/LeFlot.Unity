﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testSpatial : MonoBehaviour
{
    public MainScript mainScript;
    public bool loop;
    public float loopLength;
    public bool special;
    public int pitch;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loopFunc());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator loopFunc()
    {
        while (loop)
        {
            if (special)
            {
                //int pitch = Random.Range(0, 6);
                mainScript.SendSound(pitch, gameObject, true);
            } else
            {
                //int pitch = Random.Range(0, 12);
                mainScript.SendSound(pitch, gameObject);
            }
            yield return new WaitForSeconds(loopLength);
        }
    }
}
