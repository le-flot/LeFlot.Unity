﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxVelocity : MonoBehaviour
{
    private Rigidbody _rigidbody;
    public float maxVelocity = 10;
    public float brake = 0.5f;

    void Start()
    {
        _rigidbody = transform.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (_rigidbody.velocity.sqrMagnitude > maxVelocity)
        {
            //smoothness of the slowdown is controlled by brake, 
            //0.5f is less smooth, 0.9999f is more smooth
            _rigidbody.velocity *= brake;
        }
    }
}
