﻿using UnityEngine;

public class ScreenCapturer : MonoBehaviour
{

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ScreenCapture.CaptureScreenshot("Captures/capture-" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png");
            Debug.Log("captured image at " + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        }
    }

}
