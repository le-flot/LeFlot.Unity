﻿using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using System.Linq;
using LeFlot;
using LeFlot.Models;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class LeFlotObjectSpawner : MonoBehaviour
{
    public LeFlotClient client;
    public ThemeManager themeManager;
    public GameObject textObject;

    [Header("Objects creation")]
    public float posX;
    public float posY;
    public float posZMin;
    public float posZMax;
    public float avoidPosZMin;
    public float avoidPosZMax;

    [Header("Objects maximum velocity")]
    public float maxVelocity;
    public float brake = 0.5f;

    private bool _flipSide;

    [Header("Objects destruction")]
    public float maxX = 7.0f;
    public float immobileThreshold = 0.1f;

    [Header("Spawn settings")]
    public bool spawnModels = true;
    public int maxSpawnedModels = 20;
    public float freqMinInit = 1.0f;
    public float freqMaxInit = 3.0f;
    public float freqMin = 1.0f;
    public float freqMax = 3.0f;

    [Header("Spawn settings > Custom")]
    public int maxCustomModels = 6;
    public int maxCustomModelsPerMessage = 2;
    public string[] collaboratorsList;
    private int _collabId = 0;
    private bool _alternate;

    private readonly List<MessageModel> _leFlotMessages = new List<MessageModel>();
    private readonly List<SpawnedModel> _spawnedModels = new List<SpawnedModel>();
    private readonly List<SpawnedModel> _spawnedIndicationModels = new List<SpawnedModel>();
    private readonly Dictionary<int, List<SpawnedModel>> _spawnedCustomModels = new Dictionary<int, List<SpawnedModel>>();

    private void Start()
    {
        if (client == null || themeManager == null)
        {
            Debug.LogError($"{this}: The component is not properly configured.");
            return;
        }

        freqMin = freqMinInit;
        freqMax = freqMaxInit;

        StartCoroutine(SpawnModels());
    }

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.J))
        {
            foreach (var pair in _spawnedCustomModels)
            {
                Debug.Log(pair.Key + " :: " + pair.Value);
            }
        }

            if (!client.IsReady)
        {
            return;
        }

        // Get new messages.
        var newFlotMsgs = client.messages.Except(_leFlotMessages).ToArray();

        foreach (var flotMsg in newFlotMsgs)
        {
            _leFlotMessages.Add(flotMsg);
        }
    }

    private IEnumerator SpawnModels()
    {
        while (true)
        {
            // Wait first (it lets time for LeFlotClient to get the data from the server).
            yield return new WaitForSeconds(Random.Range(freqMin, freqMax));

            if (spawnModels)
            {
                if (_spawnedModels.Count < maxSpawnedModels)
                {
                    SpawnedModel spawnedModel = null;
                    if (_spawnedCustomModels.Count < maxCustomModels)
                    {
                        spawnedModel = SpawnCustomModel();
                    }

                    if (_spawnedIndicationModels.Count < 1)
                    {
                        if (_alternate)
                        {
                            spawnedModel = SpawnSiteModel();
                            _alternate = !_alternate;
                        }
                        else
                        {
                            spawnedModel = SpawnCollabModel();
                            _alternate = !_alternate;
                        }

                        _spawnedIndicationModels.Add(spawnedModel);
                    }

                    if (spawnedModel == null)
                    {
                        SpawnBotModel();
                    }
                }
            }
        }
    }

    private SpawnedModel SpawnCustomModel()
    {
        var theme = themeManager.CurrentTheme;
        if (theme == null)
        {
            return null;
        }

        var spawnMessage = GetMostImportantMessage();
        if (spawnMessage == null)
        {
            return null;
        }

        SpawnedModel spawnedModel = null;
        if (spawnMessage != null)
        {
            var model = theme.models.ElementAtOrDefault(spawnMessage.objectId);
            if (model == null)
            {
                // Model ID out of bound, pick a random model.
                model = theme.models.ElementAt(Random.Range(0, theme.models.Length));
            }

            var soundPitch = Random.Range(0, 6);
            spawnedModel = SpawnModel(model, spawnMessage.text, true, soundPitch);
            
            if (!_spawnedCustomModels.TryGetValue(spawnMessage.id, out var messageSpawnedModels))
            {
                messageSpawnedModels = new List<SpawnedModel>();
                messageSpawnedModels.Add(spawnedModel);
                _spawnedCustomModels.Add(spawnMessage.id, messageSpawnedModels);

            }
            messageSpawnedModels.Add(spawnedModel);
        }

        return spawnedModel;
    }

    private SpawnedModel SpawnSiteModel()
    {
        var theme = themeManager.CurrentTheme;
        if (theme == null)
        {
            return null;
        }

        var idModel = Random.Range(0, theme.models.Length);
        var model = theme.models.ElementAtOrDefault(idModel);
        var siteStr = "Allez à LeFlot.art pour lancer votre message";
        var soundPitch = Random.Range(0, 6);

        return SpawnModel(model, siteStr, true, soundPitch);
    }

    private SpawnedModel SpawnCollabModel()
    {
        var theme = themeManager.CurrentTheme;
        if (theme == null)
        {
            return null;
        }

        var idModel = Random.Range(0, theme.models.Length);
        var model = theme.models.ElementAtOrDefault(idModel);

        var collabStr = collaboratorsList[_collabId];
        _collabId = (_collabId + 1) % collaboratorsList.Length;
        collabStr = collabStr.Replace("\\n", "\n");
        var soundPitch = Random.Range(0, 6);

        return SpawnModel(model, collabStr, true, soundPitch);
    }

    private SpawnedModel SpawnBotModel()
    {
        var theme = themeManager.CurrentTheme;
        if (theme == null || !theme.spawnBots)
        {
            return null;
        }

        var model = theme.models.ElementAt(Random.Range(0, theme.models.Length));
        var soundPitch = Random.Range(0, 12);

        return SpawnModel(model, string.Empty, false, soundPitch);
    }

    private SpawnedModel SpawnModel(GameObject model, string text, bool custom, int soundPitch)
    {
        // Debug.Log($"Selected model: {model.name}");

        var theme = themeManager.CurrentTheme;
        if (theme == null)
        {
            Debug.LogError("Can't spawn model: no current theme detected.");
            return null;
        }

        // Spawn model.
        var gameObj = Instantiate(model, Vector3.zero, model.transform.localRotation, transform).gameObject;
        gameObj.transform.localPosition = GetModelInitPos(gameObj);
        gameObj.GetComponentInChildren<SendCollisions>().soundPitch = soundPitch;

        if (gameObj.TryGetComponent<DestroyObject>(out var destroyObjectComponent))
        {
            destroyObjectComponent.spawner = this;
        }

        if (gameObj.TryGetComponent<MaxVelocity>(out var velocityScript))
        {
            velocityScript.maxVelocity = maxVelocity;
            velocityScript.brake = brake;
        }

        var spawnedModel = gameObj.AddComponent<SpawnedModel>();

        // Get color from the theme.
        var modelColor = theme.GetColor(gameObj);
        GameObject textGameObject = null;

        if (text.Length > 0)
        {
            // Add text object.
            textGameObject = Instantiate(textObject, transform).gameObject;
            textGameObject.transform.position = gameObj.transform.position;
            textGameObject.name = $"{gameObj.name}_text";

            var tmpComponent = textGameObject.GetComponent<TextMeshPro>();
            tmpComponent.SetText(text);
            tmpComponent.color = modelColor;

            var followScriptComponent = textGameObject.GetComponent<FollowScript>();
            followScriptComponent.toFollow = gameObj;

            spawnedModel.TextObject = textGameObject;
        }

        // send the text object to the theme for custom actions
        if (textGameObject)
        {
            theme.SpawnedText(textGameObject);
        }

        // send the object to the theme for custom actions
        theme.SpawnedObject(gameObj, modelColor, custom);

        _spawnedModels.Add(spawnedModel);

        return spawnedModel;
    }

    private MessageModel GetMostImportantMessage()
    {
        if (!_leFlotMessages.Any())
        {
            return null;
        }

        // Prioritize least shown messages.
        MessageModel spawnMessage = null;
        for (var i = 0; i < maxCustomModelsPerMessage; ++i)
        {
            spawnMessage = _leFlotMessages.LastOrDefault(x =>
            {
                return !_spawnedCustomModels.TryGetValue(x.id, out var spawnedMessages) ||
                    spawnedMessages.Count == i;
            });

            if (spawnMessage != null)
            {
                // Found a message that is either not spawned
                // or the least spawned of the batch.
                break;
            }
        }

        return spawnMessage;
    }

    // Choose a spawn position for the model that is not in the way of the
    // furnitures or any other objects.
    private Vector3 GetModelInitPos(GameObject model)
    {
        // Find min/max Z position.
        float minInitPosZ;
        float maxInitPosZ;
        if (_flipSide)
        {
            minInitPosZ = posZMin;
            maxInitPosZ = avoidPosZMin;
        }
        else
        {
            minInitPosZ = avoidPosZMax;
            maxInitPosZ = posZMax;
        }

        // Get first position to try.
        var pos = new Vector3(posX, posY, Random.Range(minInitPosZ, maxInitPosZ));

        var renderer = model.GetComponentInChildren<Renderer>();
        if (renderer == null)
        {
            return pos;
        }

        // Try new pos as many times as the model fits in the distance.
        var modelSize = Mathf.Max(renderer.bounds.size.z, 0.2f);
        var nbTries = Mathf.Ceil((maxInitPosZ - minInitPosZ) / modelSize);

        var zOffset = 0.0f;
        var collision = false;
        do
        {
            // Get initial position.
            pos.z += zOffset;

            int layerMask = 1 << 10;
            collision = Physics.CheckBox(
                pos,
                renderer.bounds.extents,
                Quaternion.identity,
                layerMask,
                QueryTriggerInteraction.Ignore);

            if (collision)
            {
                // There's a collision, shift right until there's some clear space.
                zOffset += modelSize;
                if (pos.z + zOffset > maxInitPosZ)
                {
                    zOffset -= maxInitPosZ - minInitPosZ;
                }
            }

            --nbTries;
        } while (nbTries > 0 && collision);

        if (nbTries == 0)
        {
            Debug.LogWarning($"[Object: {model.name}] Could not find a clear position, spawning at {pos} anyway.");
        }

        _flipSide = !_flipSide;

        return pos;
    }

    public void DestroySpawnedModel(SpawnedModel component)
    {
        if (component == null)
        {
            return;
        }

        // Search object in spawned models list.
        var spawnedModelIdx = _spawnedModels.FindIndex(x => x == component);
        if (spawnedModelIdx == -1)
        {
            Debug.Log($"DestroySpawnedModel has been called with unkown GameObject \"{component.gameObject.name}\".");
            return;
        }

        // Remove it from list.
        _spawnedModels.RemoveAt(spawnedModelIdx);

        // Search object in spawned indication models list.

        var spawnedIndicationModelIdx = _spawnedIndicationModels.FindIndex(x => x == component);

        if (spawnedIndicationModelIdx != -1)
        {
            _spawnedIndicationModels.RemoveAt(spawnedIndicationModelIdx);
        }

        // Search object in custom spawned models and, if found, remove it.
        var listContainingComponent = _spawnedCustomModels.Values.FirstOrDefault(l => l.Any(x => x == component));
        if (listContainingComponent != null)
        {
            var item = _spawnedCustomModels.First(kvp => kvp.Value == listContainingComponent);

            _spawnedCustomModels.Remove(item.Key);
        }

        // Destroy the object.
        Destroy(component.gameObject);
    }

    public void SetNbrActivePerson(int nbrActivePerson)
    {
        if (nbrActivePerson > 0)
        {
            freqMin = freqMinInit / (float)nbrActivePerson;
            freqMax = freqMaxInit / (float)nbrActivePerson;
        }
        else
        {
            freqMin = freqMinInit;
            freqMax = freqMaxInit;
        }
    }

    public void ChangeModelsTextColor()
    {
        var theme = themeManager.CurrentTheme;
        var time = theme.interpolationTime;

        foreach (var spawnedModel in _spawnedModels)
        {
            var color = theme.GetColor(spawnedModel.gameObject);
            spawnedModel.ChangeTextColor(color, time);
        }
    }
}
