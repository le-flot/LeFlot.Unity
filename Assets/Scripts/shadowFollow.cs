﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shadowFollow : MonoBehaviour
{
    public GameObject toFollow;
    private Vector3 _initialPosition;
    private Vector3 _initialRotation;
    public bool rotationAroundY = true;

    void Start()
    {
        _initialPosition = transform.position;
        _initialRotation = transform.eulerAngles;
    }

    void Update()
    {
        transform.position = new Vector3(toFollow.transform.position.x, _initialPosition.y, toFollow.transform.position.z);
        if (rotationAroundY)
        {
            transform.rotation = Quaternion.Euler(_initialRotation.x, toFollow.transform.eulerAngles.y, _initialRotation.z);
        } else
        {
            transform.rotation = Quaternion.Euler(_initialRotation.x, _initialRotation.y, toFollow.transform.eulerAngles.z);
        }
    }
}
