﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewTheme", menuName = "ScriptableObjects/Theme", order = 1)]
public class ThemeDescriptor : ScriptableObject
{
    public string themeName;
    public Theme themePrefab;
}
