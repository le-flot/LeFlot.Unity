using UnityEngine;

public class SummerTheme : Theme
{
    public WaterVolume.WaterVolume waterVolume;
    public Renderer waterSurfaceRenderer;
    public float minCurrent = 0.1f;
    public float maxCurrent = 0.925f;
    public float minShaderspeed = 0.05f;
    public float maxShaderSpeed = 0.2f;

    private float _accumulatedMovement = 0.0f;
    private float _waterShaderSpeed = 0.0f;

    public override void SetNbrActivePerson(int nbrActivePerson)
    {
        var wantedFlowSpeed = nbrActivePerson / 4f * (maxCurrent - minCurrent) + minCurrent;
        var wantedShaderSpeed = nbrActivePerson / 4f * (maxShaderSpeed - minShaderspeed) + minShaderspeed;
        iTween.Stop(gameObject);

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", waterVolume.flow.x,
            "to", wantedFlowSpeed,
            "onupdate", nameof(CallBackModifyCurrent),
            "time", interpolationTime));

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", _waterShaderSpeed,
            "to", wantedShaderSpeed,
            "onupdate", nameof(CallBackModifyShader),
            "time", interpolationTime));
    }

    public override void SpawnedText(GameObject gameObj)
    {
        if (gameObj.name == "P_Tube(Clone)_text" || gameObj.name == "P_TubeDuck(Clone)_text")
        {
            gameObj.GetComponent<FollowScript>().offset = offsetText + new Vector3(0, 0, .2f);
        } else
        {
            gameObj.GetComponent<FollowScript>().offset = offsetText;
        }
    }

    void Update()
    {
        _accumulatedMovement += Time.deltaTime * _waterShaderSpeed * 1.0f; // Euler integration
        waterSurfaceRenderer.material.SetFloat("_GlobalTime", _accumulatedMovement);
    }

    private void CallBackModifyCurrent(float value)
    {
        waterVolume.flow = new Vector3(value, 0, 0);
        initialVelocity = new Vector3(value, 0, 0);
    }

    void CallBackModifyShader(float value)
    {
        _waterShaderSpeed = value;
    }
}
