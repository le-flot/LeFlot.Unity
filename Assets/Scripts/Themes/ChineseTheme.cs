﻿using UnityEngine;

public class ChineseTheme : Theme
{
    public WaterVolume.WaterVolume waterVolume;
    public Renderer waterSurfaceRenderer;
    public float minCurrent = 0.1f;
    public float maxCurrent = 1.2f;
    public float minShaderspeed = 0.1f;
    public float maxShaderSpeed = 1.2f;
    public float randomAngleYVelocity = 30;

    private float _accumulatedMovement = 0.0f;
    private float _waterShaderSpeed = 0.0f;

    public override void SetNbrActivePerson(int nbrActivePerson)
    {
        var wantedFlowSpeed = nbrActivePerson / 4f * (maxCurrent - minCurrent) + minCurrent;
        var wantedShaderSpeed = nbrActivePerson / 4f * (maxShaderSpeed - minShaderspeed) + minShaderspeed;
        iTween.Stop(gameObject);

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", waterVolume.flow.x,
            "to", wantedFlowSpeed,
            "onupdate", nameof(CallBackModifyCurrent),
            "time", interpolationTime));

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", _waterShaderSpeed,
            "to", wantedShaderSpeed,
            "onupdate", nameof(CallBackModifyShader),
            "time", interpolationTime));
    }

    void Update()
    {
        _accumulatedMovement += Time.deltaTime * _waterShaderSpeed * 1.0f; // Euler integration
        waterSurfaceRenderer.material.SetFloat("_GlobalTime", _accumulatedMovement);
    }

    public override void SpawnedObject(GameObject gameObj, Color color, bool custom)
    {
        base.SpawnedObject(gameObj, color, custom);

        // set initial rotation and angle velocity
        var randomZRotation = Random.Range(0, 360);
        gameObj.transform.eulerAngles = new Vector3(gameObj.transform.eulerAngles.x, randomZRotation, gameObj.transform.eulerAngles.z);
        var bodyTr = gameObj.transform.Find("Body");
        if (bodyTr != null)
        {
            var randomY = Random.Range(-randomAngleYVelocity, randomAngleYVelocity);
            gameObj.GetComponent<Rigidbody>().AddTorque(new Vector3(0, randomY, 0));
        }
    }

    private void CallBackModifyCurrent(float value)
    {
        waterVolume.flow = new Vector3(value, 0, 0);
        initialVelocity = new Vector3(value, 0, 0);
    }

    void CallBackModifyShader(float value)
    {
        _waterShaderSpeed = value;
    }
}
