﻿using UnityEngine;

public class ChristmasTheme : Theme
{
    public float minCurrent = 0.1f;
    public float maxCurrent = 1.2f;
    public float minGravityX;
    public float maxGravityX;

    private Camera _surfaceCamera;

    private void Start()
    {
        // switch surface camera to orthographic for mesh tracker
        _surfaceCamera = GameObject.Find("Camera Capture Surface").GetComponent<Camera>();
        _surfaceCamera.orthographic = true;
        AkSoundEngine.PostEvent("stop_flow", gameObject);
        SetThemeMusic();
    }

    public override void SetThemeMusic()
    {
        AkSoundEngine.SetState("theme", "christmas");
    }

    private void OnDestroy()
    {
        Physics.gravity = new Vector3(0, -9.81f, 0);
        _surfaceCamera.orthographic = false;
        AkSoundEngine.PostEvent("start_flow", gameObject);
    }

    public override void SetNbrActivePerson(int nbrActivePerson)
    {
        var wantedFlowSpeed = nbrActivePerson / 4f * (maxCurrent - minCurrent) + minCurrent;
        initialVelocity = new Vector3(wantedFlowSpeed, 0, 0);

        var gravityX = nbrActivePerson / 4f * (maxGravityX - minGravityX) + minGravityX;
        Physics.gravity = new Vector3(gravityX, -9.81f, 0);
    }

    public override void SpawnedObject(GameObject gameObj, Color color, bool custom)
    {
        SetInitialVelocity(gameObj, custom);
        SetLightsColors(gameObj, color, custom);
    }
}
