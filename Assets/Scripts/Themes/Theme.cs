using UnityEngine;
using TMPro;

public class Theme : MonoBehaviour
{
    public bool spawnBots = true;
    public GameObject[] models;
    public int madmapperScene = 1;

    [Space(20)]

    public Color lightBotColor = Color.white;
    public float customFlashIntensity = 5;
    public float customBaseLightIntensity = 1;
    public float botFlashIntensity = 2f;
    public float botBaseLightIntensity = .5f;
    public float interpolationTime = 2.0f;
    public Vector3 offsetText = new Vector3(0, 0.2f, 0.1f);

    [Space(20)]

    public Color[] customColors;

    [Space(20)]

    public Vector3 initialVelocity;

    private void Start()
    {
        SetThemeMusic();
    }

    public virtual void SetNbrActivePerson(int nbrActivePerson)
    {
    }

    public virtual void SpawnedText(GameObject gameObj)
    {
        gameObj.GetComponent<FollowScript>().offset = offsetText;
    }

    public virtual void SpawnedObject(GameObject gameObj, Color color, bool custom)
    {
        SetInitialVelocity(gameObj, custom);
        SetLightsColors(gameObj, color, custom);
    }

    public virtual Color GetColor(GameObject gameObj)
    {
        var color = customColors[Random.Range(0, customColors.Length)];
        return color;
    }

    protected virtual void SetInitialVelocity(GameObject gameObj, bool custom)
    {
        var bodyTr = gameObj.transform.Find("Body");
        if (bodyTr == null)
        {
            gameObj.GetComponent<Rigidbody>().velocity = initialVelocity;
            gameObj.GetComponent<SendCollisions>().customTag = custom;
        }
        else
        {
            bodyTr.GetComponent<Rigidbody>().velocity = initialVelocity;
            bodyTr.GetComponent<SendCollisions>().customTag = custom;
            var allRigidbodies = gameObj.GetComponentsInChildren<Rigidbody>();
            foreach (var rb in allRigidbodies)
            {
                rb.velocity = initialVelocity;
            }
        }
    }

    protected virtual void SetLightsColors(GameObject gameObj, Color color, bool custom)
    {
        var lightColor = custom ? color : lightBotColor;
        var baseLightIntensity = custom ? customBaseLightIntensity : botBaseLightIntensity;
        var flashIntensity = custom ? customFlashIntensity : botFlashIntensity;

        gameObj.GetComponentInChildren<Light>().color = lightColor;
        if (!custom)
        {
            gameObj.GetComponentInChildren<Light>().enabled = false;
        }

        var sendCollisionsComponent = gameObj.GetComponentInChildren<SendCollisions>();
        sendCollisionsComponent.baseLightIntensity = baseLightIntensity;
        sendCollisionsComponent.flashIntensity = flashIntensity;
        sendCollisionsComponent.FadeIn();
    }

    public virtual void SetThemeMusic()
    {
        AkSoundEngine.SetState("theme", "water");
    }

    public virtual void SetFlow()
    {
        AkSoundEngine.PostEvent("start_flow", gameObject);
    }
}
