using UnityEngine;

public class CanadaTheme : Theme
{
    public WaterVolume.WaterVolume waterVolume;
    public Renderer waterSurfaceRenderer;
    public float minCurrent = 0.1f;
    public float maxCurrent = 0.925f;
    public float minShaderspeed = 0.05f;
    public float maxShaderSpeed = 0.2f;
    public float randomAngleVelocity = 30;

    private float _accumulatedMovement = 0.0f;
    private float _waterShaderSpeed = 0.0f;

    public override void SetNbrActivePerson(int nbrActivePerson)
    {
        var wantedFlowSpeed = nbrActivePerson / 4f * (maxCurrent - minCurrent) + minCurrent;
        var wantedShaderSpeed = nbrActivePerson / 4f * (maxShaderSpeed - minShaderspeed) + minShaderspeed;
        iTween.Stop(gameObject);

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", waterVolume.flow.x,
            "to", wantedFlowSpeed,
            "onupdate", nameof(CallBackModifyCurrent),
            "time", interpolationTime));

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", _waterShaderSpeed,
            "to", wantedShaderSpeed,
            "onupdate", nameof(CallBackModifyShader),
            "time", interpolationTime));
    }

    public override void SpawnedObject(GameObject gameObj, Color color, bool custom)
    {
        base.SpawnedObject(gameObj, color, custom);

        // set initial rotation and angle velocity
        var randomYRotation = Random.Range(0, 360);
        gameObj.transform.eulerAngles = new Vector3(gameObj.transform.eulerAngles.x, randomYRotation, gameObj.transform.eulerAngles.z);
        gameObj.transform.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(-randomAngleVelocity, randomAngleVelocity), 0));
    }

    protected override void SetLightsColors(GameObject gameObj, Color color, bool custom)
    {
        // don't change the color of custom models
        if (!custom)
        {
            var lightColor = lightBotColor;
            gameObj.GetComponentInChildren<Light>().color = lightColor;
        }

        var baseLightIntensity = custom ? customBaseLightIntensity : botBaseLightIntensity;
        var flashIntensity = custom ? customFlashIntensity : botFlashIntensity;

        var sendCollisionsComponent = gameObj.GetComponentInChildren<SendCollisions>();
        sendCollisionsComponent.baseLightIntensity = baseLightIntensity;
        sendCollisionsComponent.flashIntensity = flashIntensity;
        sendCollisionsComponent.FadeIn();
    }

    void Update()
    {
        _accumulatedMovement += Time.deltaTime * _waterShaderSpeed * 1.0f; // Euler integration
        waterSurfaceRenderer.material.SetFloat("_GlobalTime", _accumulatedMovement);
    }

    private void CallBackModifyCurrent(float value)
    {
        waterVolume.flow = new Vector3(value, 0, 0);
        initialVelocity = new Vector3(value, 0, 0);
    }

    void CallBackModifyShader(float value)
    {
        _waterShaderSpeed = value;
    }
}
