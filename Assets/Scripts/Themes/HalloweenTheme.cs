﻿using UnityEngine;
using UnityEngine.Rendering;

public class HalloweenTheme : Theme
{

    public WaterVolume.WaterVolume waterVolume;
    public float minCurrent = 0.1f;
    public float maxCurrent = 1.2f;

    [Header("Lava")]
    public Renderer lavaRenderer;
    public float minGlobalSpeed = 0.1f;
    public float maxGlobalSpeed = 1;
    public float minEmit = 0.2f;
    public float maxEmit = 4.0f;

    private Material _lavaMaterial;
    private float _accumulatedMovement = 0.0f;
    private float _lavaShaderSpeed = 0.0f;
    private float _lavaEmit = 1.0f;

    //custom colors positions correspond to nbr active persons
    public Color customColor;

    private LeFlotObjectSpawner _spawner;
    private GameObject _floor;
    private Vector3 _prevPositionFloor;

    private void Start()
    {
        _lavaMaterial = lavaRenderer.GetComponent<Renderer>().material;
        _floor = GameObject.Find("floor").gameObject;
        _prevPositionFloor = _floor.transform.position;
        _floor.transform.position = new Vector3(_floor.transform.position.x, 1.85f, _floor.transform.position.z);
    }

    private void OnDestroy()
    {
        _floor.transform.position = _prevPositionFloor;
        AkSoundEngine.PostEvent("stop_lava", gameObject);
    }

    public override void SetNbrActivePerson(int nbrActivePerson)
    {
        iTween.Stop(gameObject);

        var wantedFlowSpeed = nbrActivePerson / 4f * (maxCurrent - minCurrent) + minCurrent;

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", waterVolume.flow.x,
            "to", wantedFlowSpeed,
            "onupdate", nameof(CallBackModifyCurrent),
            "time", interpolationTime));

        var wantedGlobalSpeed = nbrActivePerson / 4f * (maxGlobalSpeed - minGlobalSpeed) + minGlobalSpeed;

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", _lavaShaderSpeed,
            "to", wantedGlobalSpeed,
            "onupdate", nameof(CallBackModifyGlobalSpeed),
            "time", interpolationTime));

        var wantedEmit = nbrActivePerson / 4f * (maxEmit - minEmit) + minEmit;

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", _lavaEmit,
            "to", wantedEmit,
            "onupdate", nameof(CallBackModifyEmit),
            "time", interpolationTime));

        var clampedNbrActivePerson = Mathf.Clamp(nbrActivePerson, 0, 4);
        customColor = customColors[clampedNbrActivePerson];
        _spawner = GameObject.Find("LeFlotObjectSpawner").GetComponent<LeFlotObjectSpawner>();
        _spawner.ChangeModelsTextColor();
    }

    // take the color of the actual mode according to nbr active person
    public override Color GetColor(GameObject gameObj)
    {
        return customColor;
    }

    // don't change the color of the lights
    public override void SpawnedObject(GameObject gameObj, Color color, bool custom)
    {
        SetInitialVelocity(gameObj, custom);

        var baseLightIntensity = custom ? customBaseLightIntensity : botBaseLightIntensity;
        var flashIntensity = custom ? customFlashIntensity : botFlashIntensity;

        var sendCollisionsComponent = gameObj.GetComponentInChildren<SendCollisions>();
        sendCollisionsComponent.baseLightIntensity = baseLightIntensity;
        sendCollisionsComponent.flashIntensity = flashIntensity;
        sendCollisionsComponent.FadeIn();
    }

    private void Update()
    {
        _accumulatedMovement += Time.deltaTime * _lavaShaderSpeed * 1.0f;           // Euler integration
        _lavaMaterial.SetFloat("_GlobalTime", _accumulatedMovement);
        _lavaMaterial.SetFloat("_HotLavaStrenght", _lavaEmit);
    }

    private void CallBackModifyCurrent(float value)
    {
        waterVolume.flow = new Vector3(value, 0, 0);
        initialVelocity = new Vector3(value, 0, 0);
    }

    private void CallBackModifyGlobalSpeed(float value)
    {
        _lavaShaderSpeed = value;
    }

    private void CallBackModifyEmit(float value)
    {
        _lavaEmit = value;
    }

    public override void SetFlow()
    {
        AkSoundEngine.PostEvent("stop_flow", gameObject);
        AkSoundEngine.PostEvent("start_lava", gameObject);
    }

    public override void SetThemeMusic()
    {
        AkSoundEngine.SetState("theme", "halloween");
    }
}

