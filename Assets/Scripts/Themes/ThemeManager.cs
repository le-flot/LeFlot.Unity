using System.Collections;
using System.Linq;
using LeFlot;
using UnityEngine;
using UnityEngine.Events;

public class ThemeManager : MonoBehaviour
{
    public LeFlotClient client;

    public OscOut _oscOut;
    public OscOut _oscOutMad;

    public string defaultTheme = "Summer";

#if UNITY_EDITOR
    [Tooltip("Override the theme returned by the API (use only for debug).")]
    public string themeOverride;
#endif // UNITY_EDITOR

    public ThemeDescriptor[] themes;

    public ThemeDescriptor CurrentThemeDescriptor => _currentThemeDescriptor;

    public Theme CurrentTheme => _currentTheme;

    public UnityAction OnThemeChanged;

    private ThemeDescriptor _currentThemeDescriptor;
    private Theme _currentTheme;

    private void Start()
    {
        _oscOut.Open(_oscOut.port);
        _oscOutMad.Open(_oscOutMad.port);
        StartCoroutine(GetCurrentThemeLoop());
    }

    private IEnumerator GetCurrentThemeLoop()
    {
        while (true)
        {
            var themeDescriptor = GetCurrentThemeDescriptor();
            if (themeDescriptor != null && _currentThemeDescriptor != themeDescriptor)
            {
                Debug.Log($"Theme change detected. Old theme: {_currentThemeDescriptor?.themeName ?? "<none>"}, new theme: {themeDescriptor.themeName}");
                var theme = Object.Instantiate<Theme>(themeDescriptor.themePrefab);
                if (theme != null)
                {
                    var oldTheme = _currentTheme;

                    _currentThemeDescriptor = themeDescriptor;
                    _currentTheme = theme;
                    SendThemeToVVVV();

                    if (oldTheme != null)
                    {
                        Object.Destroy(oldTheme.gameObject);
                    }

                    OnThemeChanged();
                }
                else
                {
                    Debug.LogError($"Could not load theme {themeDescriptor.themeName}");
                }
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    public void SendThemeToVVVV()
    {
        _oscOut.Send("/themes", GetCurrentThemeDescriptor().themeName);
    }

    private ThemeDescriptor GetCurrentThemeDescriptor()
    {
        ThemeDescriptor themeDescriptor = default;

#if UNITY_EDITOR
        if (!string.IsNullOrWhiteSpace(themeOverride))
        {
            themeDescriptor = themes.FirstOrDefault(x => x.themeName == themeOverride);
            if (themeDescriptor != null)
            {
                return themeDescriptor;
            }
        }
#endif // UNITY_EDITOR

        if (client.IsReady)
        {
            themeDescriptor = themes.FirstOrDefault(x => x.themeName == client.currentTheme.name);
            if (themeDescriptor == null)
            {
                Debug.LogWarning($"Could not find theme \"{client.currentTheme.name}\".");
            }
        }

        if (themeDescriptor == null)
        {
            themeDescriptor = themes.FirstOrDefault(x => x.themeName == defaultTheme);
            if (themeDescriptor == null)
            {
                Debug.LogError($"Could not find default theme \"{defaultTheme}\". The script won't be able to spawn objects.");
            }
        }

        return themeDescriptor;
    }

    public void setMadmapperScene(int sceneNbr)
    {
        _oscOutMad.Send("/cues/selected/columns/"+sceneNbr, 1);
    }
}
