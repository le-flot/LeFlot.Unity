using UnityEngine;

public class FallTheme : Theme
{
    public WaterVolume.WaterVolume waterVolume;
    public Renderer waterSurfaceRenderer;
    public float minCurrent = 0.1f;
    public float maxCurrent = 1.2f;
    public float minLeavesSpeed = 0.1f;
    public float maxLeavesSpeed = 1.7f;
    public float minWindSpeed = 0.01f;
    public float maxWindSpeed = 0.7f;
    public float minLeavesEmission = 6;
    public float maxLeavesEmission = 12;
    public float randomAngleVelocity = 30;

    public float minShaderspeed = 0.1f;
    public float maxShaderSpeed = 1.2f;

    private float _accumulatedMovement = 0.0f;
    private float _waterShaderSpeed = 0.0f;
    public ParticleSystem[] leavesParticles;
    public WindZone windZone;

    void Update()
    {
        _accumulatedMovement += Time.deltaTime * _waterShaderSpeed * 1.0f; // Euler integration
        waterSurfaceRenderer.material.SetFloat("_GlobalTime", _accumulatedMovement);
    }

    public override void SetNbrActivePerson(int nbrActivePerson)
    {
        var wantedFlowSpeed = nbrActivePerson / 4f * (maxCurrent - minCurrent) + minCurrent;
        var wantedShaderSpeed = nbrActivePerson / 4f * (maxShaderSpeed - minShaderspeed) + minShaderspeed;
        iTween.Stop(gameObject);

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", waterVolume.flow.x,
            "to", wantedFlowSpeed,
            "onupdate", nameof(CallBackModifyCurrent),
            "time", interpolationTime));

        // leaves speed
        wantedFlowSpeed = nbrActivePerson / 4f * (maxLeavesSpeed - minLeavesSpeed) + minLeavesSpeed;
        var mainMod = leavesParticles[0].main;
        var initialSpeed = mainMod.startSpeed.constant;

        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", initialSpeed,
            "to", wantedFlowSpeed,
            "onupdate", nameof(CallBackModifyLeavesSpeed),
            "time", interpolationTime*2));

        //current speed
        iTween.ValueTo(gameObject,
            iTween.Hash(
            "from", _waterShaderSpeed,
            "to", wantedShaderSpeed,
            "onupdate", nameof(CallBackModifyShader),
            "time", interpolationTime));

        // leaves emission
        var wantedEmission = nbrActivePerson / 4f * (maxLeavesEmission - minLeavesEmission) + minLeavesEmission;
        foreach (ParticleSystem ps in leavesParticles)
        {
            var emissionMod = ps.emission;
            emissionMod.rateOverTime = new ParticleSystem.MinMaxCurve(wantedEmission);
        }

        // wind speed
        windZone.windMain = nbrActivePerson / 4f * (maxWindSpeed - minWindSpeed) + minWindSpeed;
    }

    public override void SpawnedObject(GameObject gameObj, Color color, bool custom)
    {
        base.SpawnedObject(gameObj, color, custom);

        // set initial rotation and angle velocity
        var randomYRotation = Random.Range(0, 360);
        gameObj.transform.eulerAngles = new Vector3(gameObj.transform.eulerAngles.x, randomYRotation, gameObj.transform.eulerAngles.z);
        gameObj.transform.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(-randomAngleVelocity, randomAngleVelocity), 0));
    }

    public override Color GetColor(GameObject gameObj)
    {
        var outlineTr = gameObj.transform.Find("Outline");
        var color = new Color();
        if (outlineTr != null)
        {
            color = outlineTr.GetComponent<Renderer>().material.GetColor("_TintColor");
        }
        color *= 1.2f;
        return color;
    }

    protected override void SetInitialVelocity(GameObject gameObj, bool custom)
    {
        // TODO: why not calling SetInitialVelocity here?
        Transform bodyTr = gameObj.transform.Find("Body");
        bodyTr.GetComponent<Rigidbody>().velocity = initialVelocity;
        bodyTr.GetComponent<SendCollisions>().customTag = custom;
    }

    private void CallBackModifyCurrent(float value)
    {
        waterVolume.flow = new Vector3(value, 0, 0);
        initialVelocity = new Vector3(value, 0, 0);
    }

    private void CallBackModifyLeavesSpeed(float value)
    {
        foreach (ParticleSystem ps in leavesParticles)
        {
            var mainMod = ps.main;
            mainMod.startSpeed = value;
        }
    }

    void CallBackModifyShader(float value)
    {
        _waterShaderSpeed = value;
    }
}
