﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class receiveOSC : MonoBehaviour
{
    [SerializeField] OscIn _oscIn;
    public GameObject scriptsGameobject;
    private MainScript _mainScript;
    public ThemeManager _themeManagerScript;

    void Start()
    {
        // Ensure that we have a OscIn component and start receiving on port 7001.
        if (!_oscIn) _oscIn = gameObject.AddComponent<OscIn>();
        _oscIn.Open(7001);
        _mainScript = scriptsGameobject.GetComponent<MainScript>();
    }

    void OnEnable()
    {
        _oscIn.MapFloat("/nbrperson", changeNbrPerson);
        _oscIn.MapFloat("/gettheme", getTheme);
    }

    void changeNbrPerson(float value)
    {
        //Debug.Log((int)value);
        _mainScript.NbrActivePerson = (int)value;
    }

    void getTheme(float value)
    {
        _themeManagerScript.SendThemeToVVVV();
        Debug.Log("VVVV asked for the current theme");
    }

}
