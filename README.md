# Le Flot

Le Flot est un projet artistique, multimédia et interactif sponsorisé par la
ville de Sherbrooke.

# Développement

## Prérequis

Voici la liste des logiciels nécessaires pour contribuer au projet:

* Git
* Git LFS
* Unity 2019.3.5f1
* VVVV voir dépôt spécifique
* Wwise 2019.2.7.7402
* MadMapper 4.0.8
* Node-Red 2.0.5 (node.js 14.17.1)
* pm2 4.5.6

## Mise en place

Cloner le dépôt Git (depuis SourceTree si vous utilisez cette interface).

Si ce n'est pas encore fait, ou si vous n'êtes plus sûr de l'avoir fait,
activer Git LFS en lançant la commande:

    git lfs install

### Unity

Installer Unity avec le logiciel Unity Hub. Il faut ensuite cliquer sur
download archive pour accéder à un lien vers la version spécifique 2019.4.19f1.

## Démarrer le backend

Le project LeFlot.Unity communique avec un backend dont le dépôt se trouve ici:
https://gitlab.com/le-flot/LeFlot.API

Une fois le dépôt cloné, voici la commande pour démarrer le serveur:

    dotnet run

## Démarrer le frontend

Pour interagir avec le backend, il existe un frontend dont le dépôt se trouve ici:
https://gitlab.com/le-flot/LeFlot.WebUI

Une fois le dépôt cloné, voici la commande pour démarrer le serveur:

    dotnet run --urls "http://*:8080"
